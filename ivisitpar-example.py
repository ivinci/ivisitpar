#!/usr/bin/env python3
import os
import sys

f = open( "ivi.rc.sample", "r")
lines = f.readlines()
for line in lines:
  (unset, rest) = line.split(maxsplit=1)
  if unset != "unset":
    (key, _, value) = line.partition("=")
    if len( key ) == 0:
      break
    value, _, _ = value.partition( "##" )
    value = value.strip( " " )
    if len( value ) > 1 and (value[ 0 ] == "'" or value[ 0 ] == '"' ):
      value = value[1:-1]
    #print( f'Setting {key} to [{value}]' )
    os.environ[key] = value
  else:
    key,_,_ = rest.partition( "##" )
    #print( f'Unsetting {key}' )
    try:
      del os.environ[key]
    except KeyError:
      pass
