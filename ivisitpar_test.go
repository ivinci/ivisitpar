package ivisitpar

import (
  "os"
  "testing"
  //"fmt"
  //"log"
)

func TestMain(m *testing.M) {
   os.Exit(m.Run())
}

func TestIvsitpar( t *testing.T ) {
  p, err := Unmarshal( "ivi.rc.sample" )
  if err != nil {
      t.Error( "Cannot open file" )
      return
   }
  if len( p.Actuals ) < 2 {
      t.Error( "Too little entries read" )
      return
  }
  if v, err := p.Get( "ZZZZ" ); err != nil {
      t.Error( "Bad getting ZZZZ from sitpar" )
      return
  } else {
    if v != "AAA" {
      t.Errorf( "Bad value for ZZZZ, expect AAA, saw [%s]", v )
      return
    }
  }
  //log.Printf( "returned from Unmarshal: %v\n", p )
  if err = p.Marshal(); err != nil {
      t.Error( "Bad writing of ivisitpar" )
      return
  }
  pp, err := Unmarshal( "ivi.rc.sample" )
  if err != nil {
      t.Error( "Cannot open file" )
      return
   }
  pp.Unset( "BOX" )
  pp.Marshal()
  if _, err := pp.Get( "BOX" ); err == nil {
      t.Error( "Can find sitpar BOX, should not be set!" )
      return
  } else {
    pp, _ = Unmarshal( "ivi.rc.sample" )
    pp.Set( "BOX", "ÖOPSÖOPS" )
    if err := pp.Marshal(); err != nil {
      t.Error( "Cannot marshal sitpars" )
    }
  }
  if _, err := pp.Get( "BLURP" ); err != nil {
    if err != Unset {
      t.Error( "Get of unset var didnt result in Unset" )
    }
  }
}
