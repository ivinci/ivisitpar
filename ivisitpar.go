package ivisitpar

import (
      "log"
      "os"
      "regexp"
      "errors"
      "strings"
      "fmt"
      //"encoding"
      "time"
      "sort"
)

type Sitpar struct {
  Par         string  // the name of the parameter, actual key
  Value       string  // the actual value for this sitpar nothing for unset, "" for empty, "<anything>" for some value
  Description string  // description of this sitpar
  Seplocation int     // location of the seperator indicating start of description
  Isset       bool
}
const descrstart = " ##"
// we go from left to right, removing the stuff just matched.
// This makes the regexp's simpler, as we only have to take stuff
// to the right into account
var isunset     = regexp.MustCompile( "^unset ([[:word:]]+)" )
var parname     = regexp.MustCompile( "([[:word:]]+)=" )
var parvalue    = regexp.MustCompile( "(.*)" + descrstart )
// the rest is description

type Sitpars struct {
   Actuals     map[string]*Sitpar
   Filename    string
   Timestamp   time.Time
}

func Unmarshal( fn string ) (*Sitpars, error) {
  fh, err := openSitParFile( fn, os.O_RDONLY )
  if err != nil {
    log.Printf( "Error opening file: %v", err )
    return nil, err
  }
  defer fh.Close()
  sp := &Sitpars{ Filename: fn,
                  Actuals: make( map[string]*Sitpar, 1 ),
         }
  if finfo, err := fh.Stat(); err != nil {
    log.Printf( "Error stat-ing file: %v", err )
    return nil, err
  } else {
    sp.Timestamp = finfo.ModTime()
  }
  for line, err := SpReadline( fh );
      err == nil;
      line, err = SpReadline( fh ) {
    //log.Printf( "read: [%s]", line )
    isanunset := isunset.FindStringSubmatch( line )
    if len( isanunset ) > 1 {
      rest := strings.Replace( line, isanunset[ 0 ], "", 1 )
      rest  = strings.Replace( rest, descrstart, "", 1 )
      sitpar := &Sitpar{ Par: strings.TrimSpace( isanunset[ 1 ] ),
                         Isset : false,
                         Description: strings.TrimSpace( rest ),
                         Seplocation: strings.Index( line, descrstart ),
                }
      os.Unsetenv( sitpar.Par )
      sp.Actuals[ isanunset[ 1 ] ] = sitpar
    } else {
      name := parname.FindStringSubmatch( line )
      if len( name ) < 2 {
        continue
      }
      rest := strings.Replace( line, name[ 0 ], "", 1 )
      value := parvalue.FindStringSubmatch( rest )
      if value == nil { // possibly no descrption, try again with empty one
        rest = rest + " ## "
        value = parvalue.FindStringSubmatch( rest )
      }
      if value == nil || len( value ) < 2 {
        log.Printf( "skipping: %s, as %s has no value", line, rest )
        continue
      }
      rest = strings.Replace( rest, value[ 0 ], "", 1 )
      thevalue := strings.TrimSpace( value[ 1 ] )
      if len( thevalue ) > 0 && (thevalue[ 0 ] == '"' || thevalue[ 0 ] == '\'') { // strip quotes, any quotes
        thevalue = thevalue[ 1:len( thevalue ) - 1 ]
      }
      rest = strings.TrimSpace( rest ) // this must be the description
      sitpar := &Sitpar{ Par: name[ 1 ],
                         Isset : true,
                         Value: thevalue,
                         Description: rest,
                         Seplocation: strings.Index( line, descrstart ),
                }
      os.Setenv( sitpar.Par, sitpar.Value )
      sp.Actuals[ name[ 1 ] ] = sitpar
    }
  }
  return sp, nil
}

func (sp *Sitpars) Marshal() error {
  fh, err := openSitParFile( sp.Filename, os.O_RDWR )
  if err != nil {
    log.Printf( "Error opening file for writing: %v", err )
    return err
  }
  defer fh.Close()
  if finfo, err := fh.Stat(); err != nil {
    log.Printf( "Error stat-ing file: %v", err )
    return err
  } else {
    if sp.Timestamp.Before( finfo.ModTime() ) {
      log.Printf( "Sitpar file to write to has been updated concurrently, fatal: %v.Before %v is true", sp.Timestamp, finfo.ModTime() )
      return errors.New( "Sitpar file to write to has been updated concurrently, fatal." )
    }
  }
  keys := make( []string, 0, len( sp.Actuals ) )
  for k, _ := range sp.Actuals {
    keys = append( keys, k )
    //log.Printf( "actual: %s", k )
  }
  sort.Strings( keys )
  fh.Truncate( 0 )
  for _, key := range keys {
    par := sp.Actuals[ key ]
    out := ""
    if par.Isset {
      out += par.Par
      out += "="
      out += "\"" + par.Value + "\""
    } else {
      out += "unset "
      out += par.Par
    }
    for n := len( out ); n <= par.Seplocation - 1; n++ {
      out += " "
    }
    if par.Description != "" {
       out += descrstart + " " // above - 1 is about this blank
    }
    out += par.Description + "\n"
    if _, err := fh.Write( []byte( out ) ); err != nil {
      log.Printf( "Error writing to file: %v", err )
    }
  }
  return nil
}

func openSitParFile( name string, mode int ) ( *os.File, error ) {
  pth := os.Getenv( "SITPARPATH" )
  if pth != "" {
    for _, val := range strings.Split( pth, ":" ) {
      if !os.IsPathSeparator( val[ len( val ) - 1 ] ) {
        val = val + string( os.PathSeparator )
      }
      if fh, err := os.OpenFile( val + name, mode, 0750 ); err == nil {
        return fh, nil
      }
    }
  } else {
    return os.OpenFile( name, mode, 0750 )
  }
  return nil, errors.New( fmt.Sprintf( "Cannot find %s in any of %s", name, pth ) )
}

func (sp *Sitpars) Set( name string, value string ) error {
   par, ok := sp.Actuals[ name ]
   if ok {
     par.Value = value
     par.Isset = true
   } else {
     sp.Actuals[ name ] = &Sitpar{ Par: name, Value: value,
                                   Isset: true, Description: "none", }
   }
   return nil
}

func (sp *Sitpars) Unset( name string ) error {
   par, ok := sp.Actuals[ name ]
   if ok {
     par.Value = ""
     par.Isset = false
   } else {
     sp.Actuals[ name ] = &Sitpar{ Par: name,
                                   Isset: false, Description: "", }
   }
   return nil
}

var Unset = errors.New( "Not set" )

func (sp *Sitpars) Get( name string ) (string, error) {
   par, ok := sp.Actuals[ name ]
   if ok && par.Isset {
     return par.Value, nil
   }
   return "", Unset
}

func SpReadline( in *os.File ) (string, error) {
  var result []byte
  terminator := byte( 0xa )
  buf := make( []byte, 1 )
  for buf[ 0 ] != terminator {
     if _, err := in.Read( buf ); err != nil {
       return "", err
     }
     result = append( result, buf[ 0 ] )
  }
  //log.Printf( "non err case, returning %s", string(result))
  if len( result ) > 0 {
    result = result[ :len( result ) - 1 ]
  }
  return string( result ), nil
}
