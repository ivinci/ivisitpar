all:
	go get
	go build

test: all
	SITPARPATH=./:../:../../:/tmp/ go test
